import { v4 as uuid } from "uuid";

const api_url = 'http://localhost:8080/api';
//const api_url='https://official-joke-api.appspot.com/jokes/ten';

export default class Controller {

    static getAll = async () => {
       let resp = await fetch(api_url+'/robots');
       if (!resp.ok){
           throw new Error('Error en fetch');
       } else {
           resp = await resp.json();
           return resp.map(el => ({...el, id: el._id}));                 
       }
    }

 
    static getGanador =  (id1,id2) => {
        const promesa = (resuelve, falla) => {
            fetch( 'http://localhost:8080/api/batallas/'+id1+'/'+id2 )
                .then(data => data.json())
                .then(robot => {
                    //robot.id = robot._id;
                    console.log("ganador",robot);
                    resuelve(robot);
                })
                .catch(err => {
                    falla(err);
                });
        };
        return new Promise(promesa);
        
     }


    static saveAll = (data) => {
        const json = JSON.stringify(data);
        localStorage.setItem('infoContactes', json);
    }


    static getById = (itemId) => {
        const promesa = (resuelve, falla) => {
            fetch(api_url + '/' + itemId)
                .then(data => data.json())
                .then(robot => {
                    robot.id = robot._id;
                    resuelve(robot);
                })
                .catch(err => {
                    falla(err);
                });
        };

        return new Promise(promesa);
    }


    static addItem = (item) => {
        const jsonRobot = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonRobot,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url, opcionesFetch)
            .then(resp => {
                console.log("robot:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));

    }



    static deleteById = (itemId) => {
        fetch(api_url + '/' + itemId, { method: 'DELETE' });
    }


}

