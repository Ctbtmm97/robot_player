import react ,{useState,useEffect} from 'react';
import Taula from './TablaElegir.jsx';
import Controller from "./JuegoController";



export default (props) => {

    const [dades, setDades] = useState([]);
  const [error, setError] = useState('');

useEffect(()=>{
    Controller.getAll()
     .then(data => setDades(data))
     .catch(err => setError(err.message));
   }, [])
   
   const columnes = [
    {
      nom: "fuerza"
    },
    {
      nom: "habilidad"

    },
    {
        nom: "velocidad"
  
      },
  
  ];
   return (
    <>
   
      <Taula datos={dades} columnas={columnes} funcion={props.funcion}  />

      {error && <h3>Error: {error}</h3>}
    </>
  );

}

