
import { Table } from "reactstrap";
import styled from 'styled-components';

import React from 'react';
import ReactDOM from 'react-dom';
import StarsRating from 'stars-rating'
import { Button } from 'reactstrap';

const Eleccion = styled.div`
display:inline-block;

border:4px black solid;
border-radius:15px;
width:400px;
height:480px;
vertical-align:top;
background-color:lightgrey;
padding:10px;
margin:10px;
`;



export default (props) => {

  const url_api = "https://robohash.org/";

  const ratingChanged = (newRating) => {
    console.log("ESTRELLAS" + newRating)
  }


  const filas = props.datos.map((el) => (


    <Eleccion key={el.id}>
      <h3 style={{ textAlign: "center" , textTransform: "capitalize"}}>{el.nombre}</h3>
      <div className="container">
      <div className="row"><img src={url_api + el.id} />
        {props.columnas.map((col, idx) =>
          <div className="col-4" >
            <span style={{ textTransform: "capitalize" }} >{col['nom'] + ": "}</span>
            <div style={{ display: "inline-block", verticalAlign: "bottom" }}><StarsRating
            count={5}
            value={el[col['nom']]}
            edit={false}
            size={16}
            color2={'#ffd700'} /></div> 
            
            </div>)}
      
        <div className="col-12"><Button  style={{margin:"10px"}}onClick={() => props.funcion(el.id)} >Elígeme!</Button></div>
      </div>
      </div>


    </Eleccion>
  ));

  return (
    <>

      <div >
        {filas}

      </div>


    </>
  );
};
