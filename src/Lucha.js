import react ,{useState,useEffect} from 'react';
import Taula from './TablaElegir.jsx';
import Controller from "./JuegoController";
import styled from 'styled-components';
import BatallaFin from './BatallaFin.jpg';
import {Button} from 'reactstrap';

export default(props)=>{
  const [robotGanador, setRobotGanador] = useState();
  const url_api="https://robohash.org/";

  const [dades, setDades] = useState([]);
  const [error, setError] = useState('');
  const [nombre, setNombre] = useState('');

useEffect(()=>{
    Controller.getAll()
     .then(data => setDades(data))
     .catch(err => setError(err.message));
   }, [])


    

  useEffect(()=>{

    console.log("ENTRANDO EN LUCHA");
    const id1 = props.robot1;
    const id2= props.robot2;
    
      
      Controller.getGanador(id1,id2)
      .then(data => setRobotGanador(data))
      .catch(err => {<h2>ERROR</h2> });
    }, [])
    console.log("DESPUES DE CONTROLLER" + robotGanador)


   

   


   
  


    return(
        <>
        <div style={{position:"relative",height:"100vh",backgroundSize:"cover" , backgroundImage: `url(${BatallaFin})`,backgroundRepeat:"no-repeat"}} ></div>
        <h1 style={{position:"absolute",top:"20%",left:"35%",color:"red",fontSize:"100px"}}>HA GANADO </h1>
        
        <div style={{position:"absolute",top:"21%",left:"33%"}}><img src={url_api+robotGanador +'.png?size=600x600'}/></div>
    
        <Button  style={{position:"absolute",top:"91%",left:"93%"}} onClick={() => props.volver()}>Volver a empezar</Button>
        
        </>

    );

}